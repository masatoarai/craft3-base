/* eslint-disable no-console */
const router = () => {
  // Window Location
  const pathName = window.location.pathname;
  const pathParams = pathName.split('/');

  // Dynamically Load Modules
  switch (pathParams[1]) {
    case '':
      System.import('./routes/home.js').then(module => {
        module.default();
      });
      break;
    default:
      console.log("Import 'default' modules");
  }
  /* eslint-enable no-console */
};

export default router;
