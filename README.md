# Craft3 Base

This is Base Craft3 template

## Installing packages

```
$ yarn
$ composer install
```

## Create .env file in root directly

Example setting here...

```
  # The environment Craft is currently running in ('dev', 'staging', 'production', etc.)
  ENVIRONMENT="dev"
  SECURITY_KEY="_gXHIYo80IuLwLX4U0YAoHlVPLDlXG2l"
  DB_DRIVER="mysql"
  DB_SERVER="localhost"
  DB_USER="root"
  DB_PASSWORD=""
  DB_DATABASE="craft3_dev"
  DB_SCHEMA="public"
  DB_TABLE_PREFIX=""
  DB_PORT=""
```

## Change webpack browserSync proxy

```
mix.browserSync({
  proxy: 'craft3-base.dev',
  files: [
    'templates/**/*.twig',
    'web/styles/**/*.css',
    'web/scripts/**/*.js',
  ],
});
```

## Deployment

Run to start development

```
$ yarn watch
```
