import FastClick from 'fastclick';
import domready from 'domready';
import ee from './components/ee';
import router from './router';

import primaryNav from './components/primary-nav';

FastClick.attach(document.body);

router();
primaryNav();

domready(() => {
  ee.emit('init');
});
