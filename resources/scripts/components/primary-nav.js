/* eslint-disable no-console */
function addEventListener() {
  console.log('Primary Nav loaded');
}

const primaryNav = () => {
  addEventListener();
};
/* eslint-enable no-console */

export default primaryNav;
